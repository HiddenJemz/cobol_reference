# COBOL Reference
To set up and configure SSH to GitLab or GitHub, follow link [Setup_SSH](https://docs.gitlab.com/ee/ssh/)\
*You'll only need to follow to the* **Configure SSH to point to a different directory** *section*

## A reference guide for COBOL programming.

* Chapter 1: Intro to COBOL programming
- [ ] ![Cobol PIC Data Types](CobolPIC.pdf)

- [ ] An interactive COBOL program
```
Refer to the calc100.cob program in the Testing folder
```
- [ ] How to code the Environment Division
```cobol
       IDENTIFICATION DIVISION.
	   PROGRAM.ID. <Name>
	   PROGRAMMER. <You>
	   COMPLETION-DATE. <Date>
	   REMARKS. <Explaination of
				 what the progam does>
```
- [ ] How to code the Working-Storage Section\
**Variables are defined in this block. Not case sensitive.**
```cobol
 WORKING-STORAGE SECTION.
*> To Comment one line at a time. Multi-line not available. 
 77	END-OF-SESSION-SWITCH	PIC X	VALUE "N".
*>
*> Two way to write decimal variables 
 77	SALES-AMOUNT			PIC 9(5)V99.
 77	SALES-TAX				PIC Z,ZZZ.99
```
- [ ] How to code the Procedure Division\
**The rules of the program is written here.**
```cobol
  PROCEDURE DIVISION.
 *> Creating a function that will loop the program until 'Y' is submitted
  000-PROGRAM-LOOP.
	PERFORM 100-Calculate-Sales-Tax
		UNTIL END-OF-SESSION-SWITCH = "Y".
	DISPLAY "Ending Calculations!".
 *> End the COBOL program session
  STOP-RUN.

 *> The actual program
  100-Calculate-Sales-Tax.
  DISPLAY "---------------------------------".
  DISPLAY "To end the sesstion, Enter 0.".
  DISPLAY "Enter the amount $" WITH NO ADVANCING.
  ACCEPT SALES-AMOUNT.
 *>
  IF SALES-AMOUNT = 0
	MOVE "Y" TO END-OF-SESSION-SWITCH
  
  ELSE
	COMPUTE SALES-TAX ROUNDED = SALES-AMOUNT * .075
  DISPLAY "The price including tax is $" SALES-TAX.
```
- [ ] Another interactive COBOL program

* Chapter 2: How to compile, test and debug
- [ ] Into to compiling & testing
```
Testing refers to finding errors.
Debugging refers to finding the cause of the error.
```
- [ ] Basic procedure for developing
- [ ] Enter and edit a source program
- [ ] Compile & run
- [ ] Review the diagnostic message for compile-time errors
- [ ] Correct compile-time errors 
- [ ] Review message for run-time errors
- [ ] Correct run-time errors
- [ ] Debugging tools
- [ ] Use COBOL statements to get debugging info.

* Chapter 3: How to write a program that prepares a report 
- [ ] Get ready for the fun.



To configure a [gitfolder](https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html#configure-git) using a terminal.

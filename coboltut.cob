           >>SOURCE FORMAT FREE
        IDENTIFICATION DIVISION.
        PROGRAM-ID. coboltut.
        AUTHOR. James Gilles.
        DATE-WRITTEN.August 21, 2021
        ENVIRONMENT DIVISION.

        DATA DIVISION.
        FILE SECTION.
        WORKING-STORAGE SECTION.
           01 UserName PIC X(30) VALUE "Me".
           01 Num1    PIC 9 VALUE ZEROS.
           01 Num2    PIC 9 VALUE ZEROS.
           01 Total   PIC 99 value 0.
           01 SSN.
               02 SSarea  PIC 999.
               02 SSgroup PIC 99.
               02 SSserial PIC 9999.
           01 PiValue CONSTANT AS 3.141579.
      *> This is a comment.

        PROCEDURE DIVISION.
        DISPLAY "What is your  name: " with no ADVANCING
        ACCEPT UserName
        DISPLAY "Please to meet you " UserName

        STOP RUN.
      *> Ensure a newLine after STOP-RUN.

       >> SOURCE FORMAT FREE
       IDENTIFICATION DIVISION.
       PROGRAM-ID. Arrays.
       ENVIRONMENT DIVISION.
       INPUT-OUTPUT SECTION.
       DATA DIVISION.
       WORKING-STORAGE SECTION.
      *> Scalar, Array, Records
      *>
      *> Level 01 and 77 can NOT use arrays
       77 user PIC 9 value zeros.
       01 myArray.
      *>
      *> Level 02-76 can USE arrays
           02 empName PIC x(11) occurs 5 times.
      *>
       PROCEDURE DIVISION.
       MAIN-ROUTINE SECTION.
       MAIN-PARA.
           DISPLAY "USING ARRAYS IN COBOL.".
      *>
      *> Inputing data into the array
           MOVE ALL spaces TO myArray.
           MOVE "James" TO empName(1).
           MOVE "Christopher" TO empName(2).
           MOVE "Boris" TO empName(3).
           MOVE "Rihanna" TO empName(4).
           MOVE "Jennifer" TO empName(5).
      *>
      *> Requesting user input
           DISPLAY "Please select a number[1 - 5]: " WITH NO ADVANCING.
           ACCEPT user.
      *> Displaying the date in the array     
           DISPLAY empName(user).
       STOP-RUN.



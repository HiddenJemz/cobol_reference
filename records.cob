       >>SOURCE FORMAT FREE
       IDENTIFICATION DIVISION.
       PROGRAM-ID. Records.
       ENVIRONMENT DIVISION.
       INPUT-OUTPUT SECTION.
       DATA DIVISION.
       WORKING-STORAGE SECTION.
      *> Scalar, Arrays, Records
      *> Level 01 and 77 can NOT be an array
       77 fromUser     PIC 9 value zeros.
       01 myRecord.
      *>
      *> Level 02 - 76 can USE the array feature
           02 empInfo  PIC x(10) OCCURS 5 TIMES.
      *> Creating records at level 5.
       01 empRecord.
               05 empID    PIC x(7).
               05 empName  PIC x(20).
               05 empDesig PIC x(30).
       PROCEDURE DIVISION.
       MAIN-ROUTINE SECTION.
       MAIN-PARA SECTION.
           DISPLAY "USING RECORDS IN COBOL.".
      *> Populating data into the myRecord
           MOVE ALL SPACES TO myRecord.
           move "James" to empInfo(1).
           MOVE "Anna" TO empInfo(2).
           MOVE "Kimberly" TO empInfo(3).
           MOVE "Timothy" TO empInfo(4).
           MOVE "Adam" TO empInfo(5).
      *> Request user input
       DISPLAY "Select a number[1 - 5]: " WITH NO ADVANCING.
       ACCEPT fromUser.
       DISPLAY empInfo(fromUser).
      *> Requesting the user to populate data into empRecord
           DISPLAY "Input Employee ID: " WITH NO ADVANCING.
           ACCEPT empID.
           DISPLAY "Input Employee Names: " WITH NO ADVANCING.
           ACCEPT empName.
           DISPLAY "Input Employee Designation: " WITH NO ADVANCING.
           ACCEPT empDesig.
      *> Show on screen
           DISPLAY empRecord.
       STOP-RUN.


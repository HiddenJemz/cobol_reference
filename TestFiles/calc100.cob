        	>>SOURCE FORMAT FREE 
       IDENTIFICATION DIVISION.
       
       PROGRAM-ID. CALC1000.
       
       ENVIRONMENT DIVISION.
       
       INPUT-OUTPUT SECTION.
        	
       DATA DIVISION.
        	
       FILE SECTION.
       	
       WORKING-STORAGE SECTION.
       
       77 END-OF-SESSION-SWITCH	PIC X	VALUE "N".
       77 SALES-AMOUNT		PIC 9(4)v99.
       77 SALES-TAX			PIC 9(4)v99.
       77 TOTAL             PIC z,zzz.99.  
       PROCEDURE DIVISION.
       
       000-CALCULATE-SALES-TAX.
        
       PERFORM 100-CALCULATE-ONE-SALES-TAX
           UNTIL END-OF-SESSION-SWITCH = "Y".
           DISPLAY "END OF SESSION".
           STOP RUN.
       
       100-CALCULATE-ONE-SALES-TAX.

       DISPLAY "-----------------------------------------".
       DISPLAY "TO END THE PROGRAM, ENTER (0) ZERO.".
       DISPLAY "ENTER THE SALES AMOUNT $" WITH NO ADVANCING.
       ACCEPT SALES-AMOUNT.
       IF SALES-AMOUNT = ZERO
           MOVE "Y" TO END-OF-SESSION-SWITCH
           
       ELSE COMPUTE SALES-TAX ROUNDED = SALES-AMOUNT * .06625
               COMPUTE TOTAL = (SALES-TAX + SALES-AMOUNT)
               DISPLAY "The government's cut is $" SALES-TAX
               DISPLAY "and your total is $" TOTAL.

